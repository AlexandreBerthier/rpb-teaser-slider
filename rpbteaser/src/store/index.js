import { createStore } from 'vuex'
import axios from 'axios';

export default createStore({
  state: {
    zoodata:[]
  },
  mutations: {
  setZooData(state,zoodata){
    this.state.zoodata = zoodata;
  }
  },
  actions: {
    loadZooData(store){
      axios
      .get('https://zoo-animal-api.herokuapp.com/animals/rand/5')
      .then(response => {
        store.commit('setZooData', response.data) 
      })
      .catch(error => {
        console.error(error)
      })
    }
  },
})
